# jupyter_pandas_base

Simple docker-based jupyter and notebook with basics of a pandas-based notebook. 

The Jupyter image comes from a pre-build docker stack. 

The only real configuration is the mapping from local sub directories into a notebook and data sub directories. 

run:   ./bin/start_jupyter.sh from root of project to get the directory mapping right.


       